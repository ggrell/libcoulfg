#############################################################################################
#
# Makefile
#
# Copyright Gilbert Grell 2020
#
#  This program is free software: you can redistribute it and/or modify it under the terms
#  of the GNU General Public License as published by the Free Software Foundation, either
#  version 3 of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
#  without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program.  If not, see <http://www.gnu.org/licenses/>.
#
#############################################################################################
#
# MAKEFILE
#
# This makefile is an addition to A.R. Barnetts COULFG code for evaluation of
# regular and irregular coulomb functions F an G that has been published in
#
# Comp. Phys. Comm. 27, 147—166 (1982), https://doi.org/10.1016/0010-4655(82)90070-4.
#
# The original code is available from within Ian Thompsons FRESCO package
# that has been published under GNU-GPL-v3.0 at github and at the fresco homepage
#
# https://github.com/I-Thompson/fresco
# http://www.fresco.org.uk/download.htm
#
# This Makefile serves the purpose to compile the COULFG code into a
# shared object library that can be used by the Auger-SCI program, which is
# published under a less restrictive licence than the GNU-GPL-v3.0, which prevents the
# direct incorporation of the COULFG code into Auger-SCI.
#
# Its primary target is the Auger-SCI code that is currently under development
#
# Author: Gilbert Grell, IMDEA nanociencia, Madrid, 2020
####################################################################################
#
# usage: with the compiler suite for the Auger compilation loaded, execute make in this directory
#
FF = $(FC)
#
# flags for compilation of the object files
# position independent code is needed, since the library might be loaded into arbitrary memory postion
FFLAGS = -O2 -fPIC
#FFLAGS = -fimplicit-none  -Wall  -Wline-truncation  -Wcharacter-truncation  -Wsurprising  -Waliasing  -Wimplicit-interface  -Wunused-parameter  -fwhole-file  -fcheck=all   -std=f2008  -pedantic  -fbacktrace -Wextra -Wimplicit-interface -fPIC -fmax-errors=1 -ggdb -fcheck=all -fbacktrace -fopenmp -O0
#
#FFLAGS = -O0 -fPIC
#
##############
# build directory:
builddir = build
##############
# include directory (.mod files)
incdir = inc
##############
# install directory
installdir = lib
##############
# code files
cde =  src/coulfg.F
#obje =  $(builddir)/coulfg.o
#
##############
##############
#
# target library name
lib = libcoulfg.so
##############
#
.PHONY : lib
lib : builddir installdir incdir  $(builddir)/$(lib) $(obje)
#
# compilation block
#
# create the shared library
# compile it directly, do not creat objects
#
# library
$(builddir)/$(lib): $(cde)# $(obje)
	$(FF) -shared $(FFLAGS) -o $@ $^ -J $(incdir)
#
# codefiles in main
#$(builddir)/%.o: src/%.f
#	$(FF) $(FFLAGS) -c $< -o $@ -J $(incdir)
#
# create the build and binary directories
builddir:
	mkdir -p $(builddir)
installdir:
	mkdir -p $(installdir)
incdir:
	mkdir -p $(incdir)

.PHONY : install
install: installdir $(builddir)/$(lib)
	cp $(builddir)/$(lib) $(installdir)/$(lib)

.PHONY : clean
clean:
	rm -rf $(builddir) ; rm -rf $(installdir) ; rm -rf $(incdir)
