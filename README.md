# LIBCOULFG: Coulomb function library #

The Auger-SCI code needs functionality to evaluate Coulomb wave functions analytically.
As such it has been using the COUL90 code by A.R. Barnett, that has been published in
chap. 9 of the book:
"Computational atomic physics: electron and positron collisions with atoms and ions"
ed. by Klaus Bartschatt, 1996, Springer, https://dx.doi.org/10.1007/978-3-642-61010-3
The relevant code, COUL90 is also available at the Fresco webpage, curated by I. Thompson:
http://www.fresco.org.uk/functions.htm

However, COUL90 is not published with a license and the "Computational atomic physics" book says:

"The programs on the enclosed disc are under copyright protection and may not be reproduced without
written peremission from Springer Verlag. One copy of the program may be made as a backup. All further copies
offend copyright law"

Which basically means that one cannot use this code in an open-source package, as Auger-SCI is.

To circumvent this issue, the public version of Auger-SCI uses the older COULFG program, written
by A.R. Barnett as well, [Comp. Phys. Comm. 27, 147—166 (1982)](https://doi.org/10.1016/0010-4655(82)90070-4).
This code is available from  within Ian Thompsons FRESCO package
either at github under GNU-GPL-v3.0:

https://github.com/I-Thompson/fresco

or under

http://www.fresco.org.uk/download.htm, also under GNU-GPL.

Since Auger-SCI uses a less restrictive open-source license than GPL-v3.0, it is not possible
to incorporate the COULFG code directly into Auger-SCI. Instead COULFG is published as a library

"LIBCOULFG", as a modified fork from FRESCO under GPLv3.0,

to  ensure its future availability and to to avoid license issues.
Auger-SCI than just links to COULFG to get the Coulomb functions.

PROSPECTS: The COUL90 code represents a further development of COULFG and it would thus be desireable to use it
instead of COULFG. A reimplementation of COUL90 to avoid the licensing trouble might be due.

## Original source code ##

The original library has been published in
[Comp. Phys. Comm. 27, 147—166 (1982)](https://doi.org/10.1016/0010-4655(82)90070-4)
and the source code is available under the GPLv3.0 license at
[github](https://github.com/I-Thompson/fresco) .

## Contributions: ##

*  All core routines of the Library have been written by A.R. Barnett (coulfg.f), and made available by
   Ian Thompson in the FRESCO respository, from which this code has been forked.
   [github repository](https://github.com/I-Thompson/fresco)

*  I (Gilbert Grell, IMDEA nanociencia Madrid, 2020) have contributed the following:
   *  the library in coulfg.f has been slightly modified to ease the interfacing to Auger-SCI
      and provide a proper balance between accuracy and computational time
   *  the installation procedures in the makefile have been supplied

## License ##

This Library is redistributed under the GPLv3.0 license as the original code by
A. R. Barnett within Ian Thompsons FRESCO repository.

## Installation and use with Auger-SCI ##

*  **This library** should be installed with the same compiler suite as the Auger-SCI
   code.
*  There is no automated setup, so you will have to adapt the makefile.
   *  prerequisites: gfortran-based mpifort compile command
   *  `cd` to the source dir of the library
   *  `make && make install` : install the library
      *  libcoulfg.so is in the lib dir.
*  The root directory of this repo is referred to as `$CFGROOT`.
*  The library is found at `$CFGROOT/lib/libcoulfg.so`.
*  To use the library, it needs to be linked against using the proper flags, which are:
   `LDFLAGS = -Wl,-rpath,$CFGROOT/lib -L$CFGROOT/lib  -lcoulfg`
   This is automated when installing Auger-SCI
   *  there are currently no automated tests. You have to rely on the testing that
      has been done during the Auger-SCI development.
   *  Auger-SCI contains tests. If they do not fail, you are probably safe.
*  **Using with Auger-SCI**:
   *  The path to this library should be specified when configuring Auger-SCI.
   *  If this is not done, Auger-SCI will download and install this library
      automatically using the recommended compiler settings
